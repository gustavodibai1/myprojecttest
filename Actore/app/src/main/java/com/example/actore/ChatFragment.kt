package com.example.actore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.fragment_chat.idBtnSendMessage
import kotlinx.android.synthetic.main.fragment_chat.idEditSendMessage

class ChatFragment : Fragment() {

    var tempKey: String? = null
    lateinit var name: String
    lateinit var list_of_rooms : ArrayList<String>

    lateinit var userName : String
    lateinit var roomName : String

    lateinit var root : DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userName = arguments?.getString("userName").toString()
        roomName = arguments?.getString("roomName").toString()

        root = FirebaseDatabase.getInstance().getReference().child(roomName)

        idBtnSendMessage.setOnClickListener { btn ->
            var map : Map<String, Any> = HashMap()
            tempKey = root.push().key
            root.updateChildren(map)

            var messageRoot : DatabaseReference = root.child(tempKey!!)
            var map2 : HashMap<String, Any> = HashMap()
            map2.put("name", userName)
            map2.put("msg", idEditSendMessage.text.toString())

            messageRoot.updateChildren(map2)
        }

        root.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, p1: String?) {
                appendChatConversation(dataSnapshot)
            }

            override fun onChildAdded(dataSnapshot : DataSnapshot, p1: String?) {
                appendChatConversation(dataSnapshot)
            }

            override fun onChildRemoved(p0: DataSnapshot) {
            }

        })
    }

    lateinit var chatMsg : String
    lateinit var chatUserName : String

    private fun appendChatConversation(dataSnapshot: DataSnapshot) {
        var i = dataSnapshot.children.iterator()

        while (i.hasNext()) {
            chatMsg = (((i.next() as DataSnapshot).value) as String)
            chatUserName = (((i.next() as DataSnapshot).value) as String)

            idTxtMessages.append("$chatUserName : $chatMsg \n")
        }
    }
}