package com.example.actore


import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import androidx.navigation.findNavController
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_home.idBtnSendMessage
import kotlinx.android.synthetic.main.fragment_home.idEditSendMessage
import kotlinx.android.synthetic.main.fragment_home.idListMessages

class HomeFragment : Fragment() {

    lateinit var name: String
    lateinit var arrayAdapter : ArrayAdapter<String>
    lateinit var listOfRooms : ArrayList<String>
    var root : DatabaseReference = FirebaseDatabase.getInstance().getReference().root

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listOfRooms = ArrayList()

        arrayAdapter = ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, listOfRooms)

        idListMessages.adapter = arrayAdapter

        requestUserName()

        idBtnSendMessage.setOnClickListener {

            var map : HashMap<String, Any> = HashMap()
            map.put(idEditSendMessage.text.toString(), "")
            root.updateChildren(map)

        }

        root.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {

                var set : HashSet<String> = HashSet()
                var i = dataSnapshot.children.iterator()

                while (i.hasNext()) {
                    set.add((i.next() as DataSnapshot).key!!)
                }

                listOfRooms.clear()
                listOfRooms.addAll(set)

                arrayAdapter.notifyDataSetChanged()
            }
        })

        idListMessages.setOnItemClickListener { parent, view, position, id ->
            val bundle = Bundle()
            bundle.putString("roomName", (view as TextView).text.toString())
            bundle.putString("userName", name)
            parent.findNavController().navigate(R.id.toChat, bundle)
        }
    }

    fun requestUserName() {
        val builder : AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.enter_name))

        val inputField : EditText = EditText(context)

        builder.setView(inputField)
        builder.setPositiveButton(getString(R.string.btn_ok)) { dialog, which ->
            name = inputField.text.toString()
        }

        builder.setNegativeButton(getString(R.string.cancel_button)) { dialog, which ->
            dialog.cancel()
            requestUserName()
        }

        builder.show()
    }
}