package com.example.actore


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : Fragment() {

    lateinit var mAuth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAuth = FirebaseAuth.getInstance()

        idBtnLogin.setOnClickListener { btn ->
            val email = idEditEmail.text.toString()
            val password = idEditPassword.text.toString()
            mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(context, getString(R.string.login_success), Toast.LENGTH_LONG).show()
                        btn.findNavController().navigate(R.id.toHome)
                    } else {
                        Toast.makeText(context, getString(R.string.login_failed), Toast.LENGTH_LONG).show()
                    }
                }
        }

        idBtnRegister.setOnClickListener { btn ->
            btn.findNavController().navigate(R.id.toRegister)
        }
    }
}
