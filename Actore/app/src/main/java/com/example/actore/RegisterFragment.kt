package com.example.actore


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_register.*


class RegisterFragment : Fragment() {

    lateinit var mAuth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAuth = FirebaseAuth.getInstance()

        idBtnRegister.setOnClickListener {
            val email = idEditEmail.text.toString()
            val password = idEditPassword.text.toString()
            val confirmPassword = idEditConfirmPassword.text.toString()
            if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()) {
                Toast.makeText(context, getString(R.string.fill_input_register), Toast.LENGTH_LONG).show()
            } else if (password == confirmPassword) {
                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(context, getString(R.string.register_success), Toast.LENGTH_LONG).show()
                        it.findNavController().navigate(R.id.toLogin)
                    } else {
                        Toast.makeText(context, getString(R.string.register_failed), Toast.LENGTH_LONG).show()
                    }
                }
            } else {
                Toast.makeText(context, getString(R.string.different_passwords), Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = mAuth.currentUser
    }
}